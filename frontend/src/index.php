<?php
include("../../backend/src/queries/noerrors.php");
?>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="
			width=device-width, 
			initial-scale=1.0" />

	<link rel="shortcut icon" href="assets/web-icon.ico" type="image/x-icon">
	<link href="https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css" rel="stylesheet" />

	<link rel="stylesheet" href="styles/bootstrap.min.css" />
	<link rel="stylesheet" href="styles/style.css" />
	<link rel="stylesheet" href="styles/switch.css" />
	<link rel="stylesheet" href="styles/loading.css" />
	<link rel="stylesheet" href="styles/results.css" />
	<link rel="stylesheet" href="styles/modal.css" />

	<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />


	<script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
	<script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
	<script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<script src="scripts/filtrado.js"></script>
	<script src="scripts/reserva.js"></script>
	<script src="scripts/mapa.js"></script>
	<script src="scripts/design.js"></script>
	<script src="index.js"></script>

	<title>MesaLibre</title>
</head>

<body>
	<header class="l-header">
		<nav class="nav">
			<div class="logo-menu__container">
				<a href="#" class="logo">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 118.87 124.29" class="logo-svg">
						<defs>
							<style>
								.cls-1 {
									fill: none;
									stroke: #ffa05c;
									stroke-linecap: round;
									stroke-miterlimit: 10;
									stroke-width: 2.5px;
								}

								.cls-2 {
									fill: #ffa05c;
								}
							</style>
						</defs>
						<title>Recurso 1</title>
						<g id="Capa_2" data-name="Capa 2">
							<g id="Icono">
								<line class="cls-1" x1="117.62" y1="1.25" x2="117.62" y2="122.98" />
								<path class="cls-2" d="M87.13,33.13a6.19,6.19,0,0,1-1.88-4.64c.09-7.11.07-14.23,0-21.35,0-4.21-1.81-5.61-5.91-4.83A5.56,5.56,0,0,0,76,3.79h0c-.58,0-1.17,0-1.38.76a12.65,12.65,0,0,0-4.37,4.56A20.26,20.26,0,0,0,66,16.86c-.69,2-1.67,2.43-3.57,1.87a34.61,34.61,0,0,0-12.89-1.4c-3.26.29-3.26.29-3.27-3.72-.11-2.85-.1-5.7-.37-8.53a2.88,2.88,0,0,0-3.06-2.93A3.63,3.63,0,0,0,38.9,4.9a10.71,10.71,0,0,0-.2,2.95c0,6-.12,11.92,0,17.88.17,7.11-2.07,13.12-7.44,17.88h0c-9.35,4.59-17.12,1.6-21-8.09a18.64,18.64,0,0,1-1.51-6.9c0-7-.1-14-.1-20.95,0-2.65-.1-5.36-3.56-5.41C1.36,2.19,1.24,5.06,1.24,7.82c0,6,0,12,0,18,0,5.58,1,11,3.88,15.78,1,1.7.57,2.92-.11,4.47-8.33,18.9-6.31,36.66,6.23,53.09,6.6,8.64,7.35,9.31,8.73,20.3.22,1.76.46,3.32,2.75,4.4a1.09,1.09,0,0,0,.27.11h0l.08,0,1.1.33A4.27,4.27,0,0,0,28.47,120v-7c9.39,4.66,19.41,5.51,29.67,4.77,5.08-1.57,10.44-2.37,15-5.33h0l4.5-2.07c.73,3.09.29,6.07.36,9,.06,2.57.84,4.63,3.72,4.56s3.57-2.19,3.54-4.74c-.06-3.87.1-7.74-.05-11.61a7.63,7.63,0,0,1,2.31-5.93C105.63,82.18,105.41,52.62,87.13,33.13Zm-75.82,16c2.4,1.23,4.62,2.89,7.33,3.4,1.42.27,1.33,1.26,1.33,2.25,0,14.31,0,28.62,0,43.06C7.06,88.06,2.79,64.13,11.31,49.14ZM75.89,89c0,5.85-.07,13.23-8.79,17-11.85,5-23.63,4.27-35.37-.94-.55-.25-1.52-.74-2.62-1.24.08-1.76.09-2.16.12-3.18.37-15.52-.37-31.06.45-46.59,2.86-.53,6.08-2.78,7.64-4.05a26.56,26.56,0,0,0,10-18.18c.23-2,.4-4.24,2.48-5.16a9.33,9.33,0,0,1,8.65.53c2.4,1.41,1.72,4,1.35,6.13a169,169,0,0,0-2,31.3A27.08,27.08,0,0,0,60.49,75.8c2.06,4.33,5.19,7.7,10.28,8.34,1.92.24,3.91.17,5.13-1.48Q75.9,85.82,75.89,89Zm0-13.51a5,5,0,0,0-1.58-1.36c-3.76-2.06-5.09-5.32-5.27-9.26a190.59,190.59,0,0,1,1.14-26.49c.76-8,3.34-15.46,6-22.91,0,1.95-.43,3.91-.44,5.84C75.67,39.36,75.91,57.41,75.92,75.47Zm9.5-32.74c10.89,10.36,12.31,35.75,0,50.6Z" />
								<path class="cls-2" d="M29.41,39.76c2.83.13,3.88-2,3.9-4.46q.09-14.37,0-28.75c0-2.5-1.08-4.5-4-4.36-2.67.13-3.41,2-3.38,4.43.05,4.73,0,9.46,0,14.19s.07,9.46,0,14.18C25.88,37.58,26.62,39.62,29.41,39.76Z" />
								<path class="cls-2" d="M17.5,39.76c2.83.1,3.82-2,3.83-4.49q.06-14.37,0-28.75c0-2.53-1.15-4.48-4-4.31-2.65.15-3.4,2-3.36,4.46.07,4.73,0,9.46,0,14.18S14,30.31,14,35C13.92,37.62,14.7,39.66,17.5,39.76Z" />
							</g>
						</g>
					</svg>
					<div class="logo-text">
						<h1>Mesa</h1>
						<h2>Libre</h2>
					</div>
				</a>
			</div>

			<div class="nav__menu" id="nav-menu">
				<ul class="nav__list">
					<li class="nav__item"><a href="#" class="nav__link">Inicio</a></li>
					<li class="nav__item"><a href="#" class="nav__link">Reservar</a></li>
					<li class="nav__item"><a href="#" class="nav__link">Contacto</a></li>
				</ul>
			</div>
			</div>

			<div class="nav__extras">
				<div class="nav__extra switch__container">
					<input type="checkbox" id="switch" />
					<label for="switch" class="lbl"></label>
				</div>
				<p class="nav__extra icon"><i class="bx bx-heart"></i></p>
				<p class="nav__extra icon"><i class="bx bx-bell"></i></p>
			</div>

			<div id="nav-toggle" class="nav__toggle">
				<i class="bx bx-menu"></i>
			</div>
		</nav>
	</header>

	<main class="main-section">
		<!-- izq -->
		<div class="global__side">
		</div>
		<div class="side">
			<!--titulo-->
			<div class="side__titular">
				<b>Filtrar Restaurante</b>
				<i class='bx bx-filter'></i>
			</div>
			<!--zona-->
			<div class="container__zona">
				<b>Zona</b>
				<select id="zona-selector" class="form-select radio__selects">
					<option>Seleccione una zona</option>
				</select>
			</div>
			<!--radios-->
			<div class="container__restos">
				<b>Restaurantes con</b>
				<div id="radio-check" name="radio-check" class="radios">
					<div class="option">
						<input type="radio" mesas-afuera="1" class="form-check-input" name="radio" id="radio1" />
						<label class="form-check-label" for="radio1">Aire Libre</label>
					</div>
					<div class="option">
						<input type="radio" mesas-afuera="0" class="form-check-input" name="radio" id="radio2" />
						<label class="form-check-label" for="radio2">Interior</label>
					</div>
					<div class="option">
						<input type="radio" mesas-afuera="1" class="form-check-input" name="radio" id="radio3" />
						<label class="form-check-label" for="radio3">Ambos</label>
					</div>
				</div>
			</div>
			<!--puntaje-->
			<div class="container__punt">
				<b>Michelines</b>
				<div class="switch_3_ways_v2" id="switch_3_ways_v2" name="as">
					<div id="1star" class="switch2 monthly">
						<i class="bx bxs-star"></i>
					</div>
					<div id="2star" class="switch2 semester">
						<i class="bx bxs-star"></i><i class="bx bxs-star"></i>
					</div>
					<div id="3star" class="switch2 annual">
						<i class="bx bxs-star"></i><i class="bx bxs-star"></i><i class="bx bxs-star"></i>
					</div>
					<div id="selector" class="selector"></div>
				</div>
			</div>
			<!--clima-->
			<div class="container__clima">
				<b>Clima</b>
				<div class="clima-widget">
					<div class="right">
						<p><i class='bx bxs-map'></i> Ubicacion</p>
						<h4>24°C</h4>
					</div>
					<div class="left">
						<i class='bx bx-sun bx-spin'></i>
						<p>Soleado</p>
					</div>
				</div>
			</div>
			<!--buscador-->
			<div class="container__buscar">
				<button class="boton" id="btn-buscar">
					<p id="btn-text">Buscar</p>
					<div class="lds-ring">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
				</button>
			</div>
		</div>

		<!-- medio -->
		<div class='arrow-bg'><i class='bx bx-right-arrow-alt arrow-volver'></i></div>
		<div style="width: 100%;" id="mapContainer" class="mapa"></div>
		<!-- der -->
		<div class="results unshow">
			<div class="result__info">
				<p>Resultados encontrados</p>
				<p id="encont">0</p>
			</div>
			<ul id="ul" class="list-group list-group-flush ull">
			</ul>
			<div id="no-res">
				<p id="p-res"></p>
			</div>
		</div>

	</main>

	<div class="modal" id="modal-reserva">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5><b>Efectua tu reserva</b></h5>
					<button type="button" class="times-btn close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="formulario">
						<div class="modal-body__top">
							<div class="img-modal"></div>
							<div class="wrapper">
								<h6>Kentucky</h6>
								<p>Localidad, Av. Direccion 1234</p>
							</div>
						</div>
						<form action="" class="modal-body__bottom">
							<div class="form-group">
								<label for="input-nombre">Nombre del titular de la reserva</label>
								<input type="text" class="form-control" id="input-nombre" placeholder="Ingrese su nombre">
							</div>
							<div class="row form-group">
								<div class="col">
									<label for="input-dni">Documento de identidad</label>
									<input type="text" class="form-control" id="input-dni" placeholder="DNI" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" />

								</div>
								<div class="col">
									<label for="input-correo">Correo</label>
									<input type="email" class="form-control" id="input-correo" placeholder="Correo eletrónico">
								</div>
							</div>
							<div class="row form-group">
								<div class="col">
									<label for="input-adultos">Número de personas</label>
									<div class="row">
										<div class="col"><input type="number" min="0" max="6" id="input-adultos" class="form-control" placeholder="Adultos"></div>
										<div class="col"><input type="number" min="0" max="6" id="input-ninos" class="form-control" placeholder="Niños"></div>
									</div>
								</div>
								<div class="col">
									<label for="input-reserva">Fecha de reserva</label>
									<input type="date" class="form-control" id="input-reserva">
								</div>
							</div>
						</form>
					</div>
					<div class="mesas">
						<div class="diagram-mesas"></div>
						<div class="ticket-mesas"></div>
					</div>
					<div class="pago">
						<div class="img-modal"></div>
						<div class="wrapper">
							<h6>Kentucky</h6>
							<p>Localidad, Av. Direccion 1234</p>
						</div>
						<div class="ticket-generado"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="boton-2 btn-danger close">Cancelar</button>
					<button type="button" class="boton-2 btn-dark">Continuar</button>
				</div>
			</div>
		</div>
	</div>

</body>

</html