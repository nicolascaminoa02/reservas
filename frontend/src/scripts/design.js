$(document).ready(function () {
	/* modo oscuro */
	function changeTheme(attribute) {
		const toggleable = document.querySelector(attribute),
			body = document.getElementsByTagName('body')

		$(toggleable).click(function () {
			$(toggleable).attr('checked', true)

			if (toggleable.checked) {
				$(body).addClass('dark')
				$(".results").addClass('dark')
			} else {
				$(body).removeClass('dark')
			}
		})
	}

	/* michelines animacion */
	const oneStar = document.getElementById('1star'),
		twoStar = document.getElementById('2star'),
		threeStar = document.getElementById('3star')

	function moveStars() {
		var starIcon = "<i class='bx bxs-star'></i>",
			selector = document.getElementById('selector')

		$(oneStar).click(() => {
			selector.style.left = 0
			selector.style.width = oneStar.clientWidth + 'px'
			selector.style.backgroundColor = '#777777'
			selector.innerHTML = starIcon
			selector.style.setProperty('height', 'inherit')
		})
		$(twoStar).click(() => {
			selector.style.left = oneStar.clientWidth + 'px'
			selector.style.width = twoStar.clientWidth + 'px'
			selector.innerHTML = starIcon + starIcon
			selector.style.backgroundColor = '#4d7ea9'
			selector.style.setProperty('height', 'inherit')
		})
		$(threeStar).click(() => {
			selector.style.left =
				oneStar.clientWidth + twoStar.clientWidth + 1 + 'px'
			selector.style.width = threeStar.clientWidth + 'px'
			selector.innerHTML = starIcon + starIcon + starIcon
			selector.style.backgroundColor = '#3167bd'
			selector.style.setProperty('height', 'inherit')
		})
	}

	moveStars()
	changeTheme('#switch')

})
