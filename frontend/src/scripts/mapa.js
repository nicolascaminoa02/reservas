$(document).ready(function () {
	var p;

	$.ajax({
		url: '../../backend/src/queries/lista_barrios.php',
		type: 'POST',
		dataType: 'json',
		success: function (output) {
			s = '';
			output.forEach( (element, value) => {
					s += 
						`<option value="${value++}">${element['Barrio']}</option>`;
			});
			$('#zona-selector').append(s);
		},
		async: false,
	});
	$.ajax({
		url: '../../backend/src/queries/lista_restaurantes.php',
		type: 'POST',
		dataType: 'json',
		success: function (output) {
			s = '';
			output.forEach(element => {
				s +=
					'<li><a class="restaurante" id="' +
					element['nombre'] +
					'">' +
					element['nombre'] +
					'</a></li>';
			});
			$('#res').append(s);
		},
		async: false,
	});

	function restrictMap(map) {
		var bounds = new H.geo.Rect(-34.7, -58.53, -34.53, -58.32);

		map.getViewModel().addEventListener('sync', function () {
			var center = map.getCenter();

			if (!bounds.containsPoint(center)) {
				if (center.lat > bounds.getTop()) {
					center.lat = bounds.getTop();
				} else if (center.lat < bounds.getBottom()) {
					center.lat = bounds.getBottom();
				}
				if (center.lng < bounds.getLeft()) {
					center.lng = bounds.getLeft();
				} else if (center.lng > bounds.getRight()) {
					center.lng = bounds.getRight();
				}
				map.setCenter(center);
			}
		});
		map.addObject(
			new H.map.Rect(bounds, {
				style: {
					fillColor: 'none',
					strokeColor: 'none',
					lineWidth: 0,
				},
			})
		);
	}

	var platform = new H.service.Platform({
		apikey: 'vuZwfqtaUxytF5eLOyW1bUnsV8DWA4Y_PcghV8hum_U',
	});

	var maptypes = platform.createDefaultLayers();

	var map = new H.Map(document.getElementById('mapContainer'), maptypes.vector.normal.map, {
		zoom: 12.5,
		center: { lng: -58.41, lat: -34.61 },
	});
	map.getBaseLayer().setMin(12);

	var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

	var ui = H.ui.UI.createDefault(map, maptypes);

	restrictMap(map);

	$('.boton').click(function () {
		map.removeObjects(map.getObjects());
		let michelin = $('#switch_3_ways_v2').val(),
      	barrio = $('#zona-selector option:selected').text(),
      	mesas = $('input:radio.form-check-input:checked').attr('mesas-afuera')
		  if (barrio == 'Seleccione una zona') barrio = ''
		  if (mesas == null) mesas = ''
		$.ajax({
			url: '../../backend/src/queries/filtrado_res.php',
			type: 'GET',
			data: `zona=${barrio}&mesas=${mesas}&estr=${michelin}`,
			dataType: 'json',
			success: function (output) {
				if (typeof output == 'string') {
					tiendas = {};
				} else {
					// console.log(output);
					tiendas = output;
					var p = [];
					const bubble = [];
					for (var i = 0; i < tiendas.length; i++) {
						var lat = tiendas[i]['latitud'];
						var lng = tiendas[i]['longitud'];
						var nombre = tiendas[i]['nombre'];
						var calle = tiendas[i]['calle'];
						var direccion = tiendas[i]['direccion'];
						p.push(new H.map.Marker({ lat: lat, lng: lng }));
						//console.log(p);
						function addMarkerToGroup(group, coordinate, html) {
							var marker = new H.map.Marker(coordinate);
							// add custom data to the marker
							marker.setData(html);
							group.addObject(marker);
						}
						function addInfoBubble(map) {
							var group = new H.map.Group();

							map.addObject(group);

							// add 'tap' event listener, that opens info bubble, to the group
							group.addEventListener(
								'tap',
								function (evt) {
									// event target is the marker itself, group is a parent event target
									// for all objects that it contains
									var bubble = new H.ui.InfoBubble(evt.target.getGeometry(), {
										// read custom data
										content: evt.target.getData(),
									});
									// show info bubble
									ui.addBubble(bubble);
								},
								false
							);

							for (x = 0; x < p.length; x++) {
								addMarkerToGroup(
									group,
									{ lat: lat, lng: lng },
									'<div><a href="https://www.mcfc.co.uk">' +
										nombre +
										'</a></div>' +
										'<div><br/>' +
										calle +
										' ' +
										direccion +
										'</div>'
								);
							}
						}

						addInfoBubble(map);
					}
					console.log(map);
				}
			},
			async: false,
		});
	});
	$('.restaurante').click(function () {
		map.removeObjects(map.getObjects());

		$.ajax({
			url: '../../backend/src/queries/lista_restaurantes.php',
			type: 'POST',
			data: { nombre: $(this).attr('id') },
			dataType: 'json',
			success: function (output) {
				if (typeof output == 'string') {
					tiendas = {};
				} else {
					tiendas = output;
					var p = [];
					const bubble = [];
					for (var i = 0; i < tiendas.length; i++) {
						var lat = tiendas[i]['latitud'];
						var lng = tiendas[i]['longitud'];
						var nombre = tiendas[i]['nombre'];
						var calle = tiendas[i]['calle'];
						var direccion = tiendas[i]['direccion'];
						p.push(new H.map.Marker({ lat: lat, lng: lng }));
						//console.log(p);
						function addMarkerToGroup(group, coordinate, html) {
							var marker = new H.map.Marker(coordinate);
							// add custom data to the marker
							marker.setData(html);
							group.addObject(marker);
						}
						function addInfoBubble(map) {
							var group = new H.map.Group();

							map.addObject(group);

							// add 'tap' event listener, that opens info bubble, to the group
							group.addEventListener(
								'tap',
								function (evt) {
									// event target is the marker itself, group is a parent event target
									// for all objects that it contains
									var bubble = new H.ui.InfoBubble(evt.target.getGeometry(), {
										// read custom data
										content: evt.target.getData(),
									});
									// show info bubble
									ui.addBubble(bubble);
								},
								false
							);

							for (x = 0; x < p.length; x++) {
								addMarkerToGroup(
									group,
									{ lat: lat, lng: lng },
									'<div><a href="https://www.mcfc.co.uk">' +
										nombre +
										'</a></div>' +
										'<div><br/>' +
										calle +
										' ' +
										direccion +
										'</div>'
								);
							}
						}

						addInfoBubble(map);
					}
					console.log(map);
				}
			},
			async: false,
		});
	});
});