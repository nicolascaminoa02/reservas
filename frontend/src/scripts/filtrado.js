$(document).ready(function () {
  var encontrados = document.getElementById('encont'),
    loading = document.getElementsByClassName('lds-ring'),
    btnBuscar = document.getElementById('btn-buscar'),
    btnText = document.getElementById('btn-text')
  const oneStar = document.getElementById('1star'),
    twoStar = document.getElementById('2star'),
    threeStar = document.getElementById('3star')

  handleReserva()
  $(btnBuscar).click(() => {
    $('.arrow-bg').css("display", "flex")
    handleListado()
    showLoading()
    aparecerResultados()
  })
  $('.arrow-bg').click(()=>{
    $('.results').removeClass('res-act')
    $('.global__side').css("display", "none")
    $('.side').removeClass('unshow')
    $('.arrow-bg').css("display", "none")
  })
  $(oneStar).click(() => $('#switch_3_ways_v2').val(1))
  $(twoStar).click(() => $('#switch_3_ways_v2').val(2))
  $(threeStar).click(() => $('#switch_3_ways_v2').val(3))

  function handleListado() {
    let michelin = $('#switch_3_ways_v2').val(),
      barrio = $('#zona-selector option:selected').text(),
      mesas = $('input:radio.form-check-input:checked').attr('mesas-afuera')

    if (barrio == 'Seleccione una zona') barrio = ''
    if (mesas == null) mesas = ''

    $.ajax({
      url: '../../backend/src/queries/filtrado_res.php',
      type: 'GET',
      dataType: 'json',
      data: `zona=${barrio}&mesas=${mesas}&estr=${michelin}`,
      success: function (output) {
        let s = ''

        $(encontrados).text(output.length)

        if (output.length === 0) {
          noRestultados()
        }

        output.forEach(e => {
          s += `
            <li class='lii'>
              <div class='left__list'>
                <img src="${e['ruta_img']}" alt=''>
              </div>
              <div class='right__list'>
                <div class='wrapper__title'>
                  <h4 class='title'>${e['nombre']}</h4>
                  <p class='stars'><i class='bx bxs-star'></i>${e['estrellas_michelin']}</p>
                </div>
                <p class='adress'>${e['barrio']}, ${e['calle']} ${e['direccion']}</p>
                <p class='tables'><i class='bx bx-chair'></i>${e['mesas']}</p>
                <div class='botones'>
                  <button
                    class='boton btn-dark btn__reservar' 
                    data-bs-toggle='modal' 
                    href='#modal'
                  >Reservar</button>
                  <button class='boton btn' id='btn-heart'><i class='bx bx-heart'></i></button>
                </div>
              </div>
            <li/>`
        })

        limpiarHTML()
        $('#ul').append(s)
        $('.btn__reservar').click(() => {
          $('#modal-reserva').addClass('show')
          leerDatosMesa()
        })
      },
    })
  }

  function handleReserva() {
    $('.close').click(() => $('#modal-reserva').removeClass('show'))
  }

  function showLoading() {
    $(loading).addClass('show')
    btnText.innerText = ''
    setTimeout(() => {
      btnText.innerText = 'Buscar'
      $(loading).removeClass('show')
    }, 2000)
  }

  function limpiarHTML() {
    if ($('#ul').length > 0) {
      $('#ul').html('')
    }
  }

  function aparecerResultados() {
    $('.side').addClass('unshow')
    $('.results').removeClass('unshow')
    $('.results').addClass('res-act')
    $('.global__side').css('width', '1%')
  }

  function noRestultados() {
    $('#ul').css('display', 'none')
    $('#no-res').css('display', 'flex')
    $('#no-res').addClass('no-res-act')
    $('#p-res').append('Lo sentimos, no existen resultados para su búsqueda')
  }

  function leerDatosMesa(mesa) {
    // const infoMesa = {
    //   imagen: mesa.querySelector('img').src,
    //   titulo: mesa.querySelector(''),
    //   direccion: ,
    // }
  }
})