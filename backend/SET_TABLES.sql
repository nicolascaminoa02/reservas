-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-11-2021 a las 03:03:15
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_reservas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurantes`
--

CREATE TABLE `restaurantes` (
  `id` int(50) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `barrio` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `calle` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` int(50) NOT NULL,
  `mesas` int(50) NOT NULL,
  `estrellas_michelin` int(50) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `mesas_afuera` tinyint(1) NOT NULL,
  `ruta_img` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `restaurantes`
--

INSERT INTO `restaurantes` (`id`, `nombre`, `barrio`, `calle`, `direccion`, `mesas`, `estrellas_michelin`, `descripcion`, `latitud`, `longitud`, `mesas_afuera`, `ruta_img`) VALUES
(1, 'Kentucky', 'Balvanera', 'Av. Rivadavia', 2402, 20, 2, 'Cadena de Restaurante de Pollo Frito', -34.609770876728355, -58.400560274739384, 1, 'imagenes/Kentucky.jpg'),
(2, 'El Español', 'Balvanera', 'Rincón', 196, 10, 2, 'Restaurante con un comedor informal donde se sirven carnes, mariscos y platos típicos españoles hasta altas horas.', -34.611337629156914, -58.39605426471372, 1, 'imagenes/ElEspanol.jpg'),
(3, 'Bellagamba', 'Balvanera', 'Av. Rivadavia', 2138, 10, 2, 'Restaurante Argentino con precios accesibles, y con un ambiente cómodo y una decoración atrayente', -34.60962130711612, -58.39666643219531, 1, 'imagenes/Bellagamba.jpg'),
(4, 'La Napolitana', 'Balvanera', 'Av. Rivadavia', 3302, 15, 2, 'Restaurante especializado en pizza, cafe y otras variedades', -34.61058336711038, -58.41298007636495, 0, 'imagenes/LaNapolitana.jpg'),
(5, 'La Conga', 'Balvanera', 'La Rioja', 39, 10, 2, 'Cocina peruana tradicional, pescado, marisco y ensaladas ecológicas en un restaurante sencillo.', -34.610772350011715, -58.40917673458172, 0, 'imagenes/LaConga.jpg'),
(6, 'Cafe los Angelitos', 'Balvanera', 'Av. Rivadavia', 2100, 16, 3, 'Cafetería llamativa y ornamentada con platos argentinos, vino de la zona y actuaciones nocturnas de tango.', -34.60943359755247, -58.396293837573985, 0, 'imagenes/LosAngelitos.jpg'),
(7, 'Sabor Norteño', 'Balvanera', 'La Rioja', 186, 20, 1, 'Restaurant Peruano para disfrutar de cenas inolvidables o de almuerzos en familia en un cálido ambiente y con un excelente servicio.', -34.6121023710305, -58.40859700520069, 1, 'imagenes/SaborNorteno.jpg'),
(8, 'Pertutti', 'Balvanera', 'Av. Corrientes', 3202, 30, 3, 'Un espacio gastronómico en el que toda la familia puede dejarse atrapar por los sabores de nuestra cocina, nuestra pastelería y el placer de sentirse a gusto y de contar ademas con un sector infantil para los mas chiquitos', -34.60408195192845, -58.41054144449033, 1, 'imagenes/Pertutti.jpg'),
(9, 'Asamble Plaza', 'Parque Chacabuco', 'Av. Asamblea', 1002, 15, 1, 'Restaurante de comida Porteña, Pizzas y Cafetería', -34.6353013637954, -58.438908816142735, 1, 'imagenes/AsambleaPlaza.jpg'),
(10, 'Fortaleza', 'Parque Chacabuco', 'Av. Del Barco Centenera', 1901, 24, 2, 'Restaurante Familiar con gran variedad de comidas', -34.64051252984914, -58.43236690694887, 0, 'imagenes/Fortaleza.jpg'),
(11, 'Siga La Vaca', 'Puerto Madero', 'Av. Alicia Moreaou de Justo', 1714, 8, 3, 'Restaurante de Menú libre (Todo incluido) y tambien  ofrece Menú a la carta, sandwichería y Take Away (Para llevar)', -34.618449518115476, -58.36509571614274, 0, 'imagenes/SigaLaVaca.jpg'),
(12, 'Rodizio', 'Puerto Madero', 'Av. Alicia Moreaou de Justo', 838, 24, 1, 'Especializado en parrilla y platos frios gourmet, este es un restaurante que no podés dejar de conocer.', -34.60714246291072, -58.366532846830836, 1, 'imagenes/Rodizio.jpg'),
(13, 'Rapanui', 'Recoleta', 'Arenales', 2302, 4, 3, 'Rapanui está pensado para disfrutar, desde los chocolates, hasta la amplia variedad de Helados, elaborados de manera artesanal con la frescura de la elaboración diaria.', -34.594043318075755, -58.3996806321687, 0, 'imagenes/Rapanui.jpg'),
(14, 'T.G.I. Fridays', 'Recoleta', 'Vicente López', 2086, 20, 3, 'Restaurante de ambiente festivo e informal, con un amplio menú de platos de cocina americana, cervezas y cócteles.', -34.5886105681095, -58.39422946163111, 1, 'imagenes/Fridays.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
