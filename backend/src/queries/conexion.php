<?php
function conexion()
{
  $server = "localhost";
  $user = "root";
  $pass = "";
  $db = "proyecto_reservas";

  $LINK = new mysqli($server, $user, $pass, $db);

  if ($LINK->connect_error) {
    die("Conexión fallida: " . $LINK->connect_error);
  }

  return $LINK;
}
