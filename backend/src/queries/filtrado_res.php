<?php
require("conexion.php");

$barrio = $_GET["zona"];
$mesasAfuera = $_GET["mesas"];
$estrellasMichelin = $_GET["estr"];

$filtradoArr = array(
	"barrio" => $barrio,
	"mesas_afuera" => $mesasAfuera,
	"estrellas_michelin" => $estrellasMichelin
);

function queryRequested()
{
	$sql =
		"SELECT * FROM `restaurantes`";
	$suma = "";

	if (!empty($GLOBALS["barrio"]) || !empty($GLOBALS["mesas_afuera"]) || !empty($GLOBALS["estrellasMichelin"]))
	{
		foreach ($GLOBALS["filtradoArr"] as $index => $dato) 
		{
			$str = "`" . $index . "`" . "=" . "'" . $dato . "' AND ";
			if ($dato == "") {
				$dato = " ";
				explode(" ", $str);
			} else {
				$suma .= $str;
			}
		}
		$res = substr($suma, 0, strlen($suma) - 5);
		$sql .= " WHERE " . $res;
		return $sql;
	}
	return $sql;
}

function crearTabla($conn, $query)
{	
	
	$resSql = $conn->query($query);
	$arr = Array();
	while($assoc = mysqli_fetch_assoc($resSql)) {
		$arr[] = $assoc;
	}	
	return json_encode($arr);
}

echo crearTabla(conexion(), queryRequested());
